﻿using System;
using System.Collections.Generic;

namespace Module3_2
{
    public static class Program
    {
        static void Main(string[] args)
        {
            //task4
            Task4 task4 = new Task4();
            Console.WriteLine("Please enter natural number to create Fibonnachi sequence.");
            if (task4.TryParseNaturalNumber(Console.ReadLine(), out int result))
            {
                Console.WriteLine(String.Format("Please see Fibonnacci sequence of {0} numbers", result));
                foreach (var item in task4.GetFibonacciSequence(result))
                {
                    Console.WriteLine(item);
                }
            }
            else
            {
                Console.WriteLine("Your input is not a natural number! Please try again.");
            }

            //task5
            Task5 task5 = new Task5();
            Console.WriteLine("Please enter number you want to reverse");
            int numberToReverse = int.Parse(Console.ReadLine());
            Console.WriteLine("Please see your number reversed");
            Console.WriteLine(task5.ReverseNumber(numberToReverse));

            //task6
            Task6 task6 = new Task6();
            Console.WriteLine("Please enter the number to create an array.");
            int arraySize = int.Parse(Console.ReadLine());
            Console.WriteLine("Please see array.");
            var array = task6.GenerateArray(arraySize);
            foreach (var item in array)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine("Please see array with opposite sign elements");

            foreach (var item in task6.UpdateElementToOppositeSign(array))
            {
                Console.WriteLine(item);
            }

            //task7
            Task7 task7 = new Task7();
            Console.WriteLine("Please enter the number to create an array.");
            int arraySize1 = int.Parse(Console.ReadLine());
            Console.WriteLine("Please see array.");

            var array1 = task7.GenerateArray(arraySize1);
            foreach (var item in array1)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine("Please see elements that are greater than previous only.");
            task7.FindElementGreaterThenPrevious(array1).ForEach(Console.WriteLine);

            //task8
            Task8 task8 = new Task8();
            Console.WriteLine("Please enter a number to create a matrix with that size.");
            int arraySize2 = int.Parse(Console.ReadLine());
            Console.WriteLine("Please see array.");
            var array2 = task8.FillArrayInSpiral(arraySize2);

            for (int x = 0; x < arraySize2; x++)
            {
                for (int y = 0; y < arraySize2; y++)
                {
                    Console.WriteLine(array2[x, y]);
                }
            }
        }
    }

    public class Task4
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            if (int.TryParse(input, out result))
            {
                return result >= 0;
            }
            else
            {
                return false;
            }
        }

        public int[] GetFibonacciSequence(int n)
        {
            List<int> fibonacciNumbers = new List<int>();
            int a = 0, b = 1;

            if (n == 0)
            {
                return fibonacciNumbers.ToArray();

            }
            if (n == 1)
            {
                fibonacciNumbers.Add(a);
                return fibonacciNumbers.ToArray();

            }
            fibonacciNumbers.Add(a);
            fibonacciNumbers.Add(b);
            for (int i = 2; i < n; i++)
            {
                int c = a + b;
                fibonacciNumbers.Add(c);
                a = b;
                b = c;
            }
            return fibonacciNumbers.ToArray();
        }
    }

    public class Task5
    {
        public int ReverseNumber(int sourceNumber)
        {
            bool isPositive = true;
            if (sourceNumber < 0)
            {
                isPositive = false;
                sourceNumber = -sourceNumber;
            }
            char[] arr = sourceNumber.ToString().ToCharArray();
            Array.Reverse(arr);
            return isPositive ? int.Parse(arr) : -int.Parse(arr);
        }
    }

    public class Task6
    {
        /// <summary>
        /// Use this method to generate array. It shouldn't throws exception.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int[] GenerateArray(int size)
        {
            if (size >=0)
            {
                int[] array = new int[size];

                Random randNum = new Random();
                for (int i = 0; i < array.Length; i++)
                {
                    array[i] = randNum.Next(-2147483648, 2147483647);
                }
                return array;
            }
            else
            {
                int[] array = new int[0];
                return array;
            }
        }

        public int[] UpdateElementToOppositeSign(int[] source)
        {
            var oppositeSignArray = Array.ConvertAll(source, i => -i);
            return oppositeSignArray;
        }
    }

    public class Task7
    {
        /// <summary>
        /// Use this method to generate array. It shouldn't throws exception.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int[] GenerateArray(int size)
        {
            if (size >= 0)
            {
                int[] array = new int[size];

                Random randNum = new Random();
                for (int i = 0; i < array.Length; i++)
                {
                    array[i] = randNum.Next(-2147483648, 2147483647);
                }
                return array;
            }
            else
            {
                int[] array = new int[0];
                return array;
            }
        }

        public List<int> FindElementGreaterThenPrevious(int[] source)
        {
            List<int> greaterIntegers = new List<int>();

            for (int i = 1; i < source.Length; i++)
            {
                if (source[i] > source[i - 1])
                {
                    greaterIntegers.Add(source[i]);
                }
            }
            return greaterIntegers;
        }
    }

    public class Task8
    {
        public int[,] FillArrayInSpiral(int size)
        {
            int[,] array = new int[size, size];
            int minRow = 0;
            int maxRow = size - 1;
            int minColumn = 0;
            int maxColumn = size - 1;
            int i = 1;

            FillToRight(array, minRow, maxRow, minColumn, maxColumn, i);

            static void FillToRight(int[,] array, int minRow, int maxRow, int minColumn, int maxColumn, int i)
            {
                if (i > array.Length * array.Length)
                {
                    return;
                }

                for (int y = minColumn; y <= maxColumn; y++)
                {
                    array[minRow, y] = i;
                    i++;
                    if (y == maxColumn)
                    {
                        minRow++;
                        FillToDown(array, minRow, maxRow, minColumn, maxColumn, i);
                    }
                }
            }

            static void FillToDown(int[,] array, int minRow, int maxRow, int minColumn, int maxColumn, int i)
            {
                if (i > array.Length * array.Length)
                {
                    return;
                }

                for (int x = minRow; x <= maxRow; x++)
                {
                    array[x, maxColumn] = i;
                    i++;
                    if (x == maxRow)
                    {
                        maxColumn--;
                        FillToLeft(array, minRow, maxRow, minColumn, maxColumn, i);
                    }
                }
            }

            static void FillToLeft(int[,] array, int minRow, int maxRow, int minColumn, int maxColumn, int i)
            {
                if (i > array.Length * array.Length)
                {
                    return;
                }

                for (int y = maxColumn; y >= minColumn; y--)
                {
                    array[maxRow, y] = i;
                    i++;
                    if (y == minColumn)
                    {
                        maxRow--;
                        FillToUp(array, minRow, maxRow, minColumn, maxColumn, i);
                    }
                }
            }

            static void FillToUp(int[,] array, int minRow, int maxRow, int minColumn, int maxColumn, int i)
            {
                if (i > array.Length * array.Length)
                {
                    return;
                }

                for (int x = maxRow; x >= minRow; x--)
                {
                    array[x, minColumn] = i;
                    i++;
                    if (x == minRow)
                    {
                        minColumn++;
                        FillToRight(array, minRow, maxRow, minColumn, maxColumn, i);
                    }
                }
            }

            return array;
        }
    }
}
